"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var login_component_1 = require("./login/login.component");
exports.LoginComponent = login_component_1.LoginComponent;
var register_component_1 = require("./register/register.component");
exports.RegisterComponent = register_component_1.RegisterComponent;
var home_component_1 = require("./home/home.component");
exports.HomeComponent = home_component_1.HomeComponent;
//# sourceMappingURL=index.js.map