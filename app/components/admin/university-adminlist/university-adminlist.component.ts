import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material';
import {MatSort} from '@angular/material';
import {PopupAdminListComponent} from './popup-adminlist/popup-adminlist.component';
import {
    HttpService,
    AlertService,
    DataSourceService,
    Database
} from "../../../services/index";

@Component({
    selector: 'app-university-order',
    templateUrl: './university-adminlist.component.html',
    styleUrls: ['./university-adminlist.component.css']
})
export class UniversityAdminListComponent implements OnInit {

    dataBase: Database | null;
    dataSource: DataSourceService | null;
    displayedColumns = ['Id', 'Name', 'Contact', 'Cabinet', 'Trade', 'Date', 'Notes', 'Approved', 'Action'];
    columnsSort = ['Id', 'Name', 'Approved'];

    pageSize: number = 5;
    pageNumber: number = 1;
    direction: string = '';
    url: string;

    @ViewChild(MatSort) sort: MatSort;

    constructor(public _dialog: MatDialog,
                private _httpService: HttpService,
                private _alertService: AlertService) {
    }

    ngOnInit() {
        this.setPage();
    }

    /**
     * Add set page
     * @param currentPage
     */
    setPage(currentPage: number = this.pageNumber) {
        Database.URL = `/admin/universityrepresentatives?PageNumber=${currentPage}&PageSize=${this.pageSize}`;
        DataSourceService.ColumnsSort = this.columnsSort;
        this.dataBase = new Database(this._httpService, this._alertService);
        this.dataSource = new DataSourceService(this.dataBase, this.sort);
    }

    /**
     * Open modal added
     */
    openPopupAdd(element: any = ''): void {
        let dialogRef = this._dialog.open(PopupAdminListComponent, {
            height: '85%',
            width: '60%',
            data: {data: element}
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.setPage();
            }
        });
    }

    /**
     * Edit element
     * @param id
     */
    edit(id: number) {
        let element = this.dataBase.data.filter(item => item.Id === id);
        element.length > 0 ? this.openPopupAdd(JSON.parse(JSON.stringify(element[0]))) : this.openPopupAdd();
    }

    /**
     * Sort Data
     * @param $event
     */
    sortData($event) {
        this.direction = $event.direction;
    }
}
