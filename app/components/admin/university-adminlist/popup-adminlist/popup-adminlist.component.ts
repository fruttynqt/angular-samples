import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {AlertService, HttpService} from '../../../../services/index';
import * as moment from 'moment';

@Component({
    moduleId: module.id,
    selector: 'app-adminlist-popup',
    templateUrl: './popup-adminlist.component.html',
    styleUrls: ['./popup-adminlist.component.css']
})
export class PopupAdminListComponent implements OnInit {
    isSpinner: boolean = false;

    model: any = {
        NameEnglish: '',
        NameHebrew: '',
        SurnameEnglish: '',
        SurnameHebrew: '',
        Birthday: '',
        Commentary: '',
        PhoneMobile: '',
        Position: '',
        PhoneWork: '',
        Email: '',
        CabinetLocation: '',
        Password: '',
        UniversityId: '',
        Approved: true,
        TradeUnionCommittee: true
    };

    universities: any[] = [];

    constructor(public dialogRef: MatDialogRef<PopupAdminListComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private _httpService: HttpService,
                private _alertService: AlertService) {
    }

    ngOnInit() {
        this._httpService.getDataAll('/universities').subscribe(
            response => {
                this.universities = response;
            },
            error => console.log('Universities: error - ' + error)
        );
        if (this.data.data) {
            this.model = this.data.data;
        }
    }

    send() {
        this.isSpinner = true;
        this.model.Birthday = moment(this.model.Birthday).format("YYYY-MM-DD");
        let body = this.model;
        this._httpService.postData('/admin/universityrepresentatives', JSON.stringify(body)).subscribe(
            response => {
                this._alertService.success('Success');
                this.isSpinner = false;
                this.dialogRef.close(true);
            },
            error => {
                let er: any = JSON.parse(error._body);
                let message = er.Message;
                if (!message) {
                    message = error.statusText;
                }
                this._alertService.error(message);
                this.isSpinner = false;
            }
        );
    }

    closePopup(): void {
        this.dialogRef.close();
    }

}
