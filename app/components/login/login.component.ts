import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

import {AlertService, AuthenticationService} from '../../services/index';

@Component({
    moduleId: module.id,
    selector: 'my-login',
    templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit {
	@Output() failedLogin = new EventEmitter <boolean>();
    model: any = {};
    returnUrl: string;

    constructor(private _route: ActivatedRoute,
                private _router: Router,
                private _authenticationService: AuthenticationService,
                private _alertService: AlertService) {
    }


    ngOnInit() {
        // reset login status
        // this._authenticationService.logout();

        // get return url from route parameters or default to '/'
        this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/order';
    }

    login() {
        this._authenticationService.login(this.model)
            .subscribe(
                data => {
                    this._router.navigate([this.returnUrl]);
                },
                error => {
                	this.failedLogin.emit(true);
                });
    }
}
