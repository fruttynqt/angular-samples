import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AlertService, HttpService, FooterService} from '../../services/index';
import {Http, Response, RequestOptions, Headers} from '@angular/http';
import {MatDialog} from '@angular/material';
import {PopupComponent} from './popup/popup.component';
import 'rxjs/add/operator/map'
import {Observable} from "rxjs/Observable";
import {apiUrl} from '../../app.config';
import * as moment from 'moment';

@Component({
    moduleId: module.id,
    templateUrl: 'register.component.html',
    styleUrls: ['./register.component.css'],
})

export class RegisterComponent implements OnInit {
    model: any = {};
    university: any = {};
    checkbox: any = {};
    universities: any[] = [];
    facultiesFirst: any[] = [];
    facultiesSecond: any[] = [];
    isRead: boolean = false;
    isSpinner: boolean = false;

    constructor(private _router: Router,
                private _http: Http,
                private _alertService: AlertService,
                private _footerService: FooterService,
                private _httpService: HttpService,
                public _dialog: MatDialog) {
    }

    ngOnInit() {
        this._footerService.content('- required fields', 'req-icon');
        this._httpService.getDataAll('/universities').subscribe(
            response => {
                this.universities = response;
            },
            error => console.log('Universities: error - ' + error)
        );
    }

    /**
     * Register
     * @returns {Subscription}
     */
    register() {
        this.isSpinner = true;
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});

        this.model.ValidationType = 1;
        this.model.Address = "test";
        this.model.Role = 0;

        if (this.model.Birthday) this.model.Birthday = moment(this.model.Birthday).format("YYYY-MM-DD");

        this.model.UniversityFirstId = this.university.UniversityFirstId.Id;

        if (this.university.UniversityFirstFacultyId)
            this.model.UniversityFirstFacultyId = this.university.UniversityFirstFacultyId.Id;

        if (this.university.UniversitySecondId)
            this.model.UniversitySecondId = this.university.UniversitySecondId.Id;

        if (this.university.UniversitySecondFacultyId)
            this.model.UniversitySecondFacultyId = this.university.UniversitySecondFacultyId.Id;

        let body = JSON.stringify(this.model);
        return this._http.post(`${apiUrl}/authorization/registration`, body, options)
            .map((response: Response) => response.json())
            .catch((error: any) => Observable.throw(error)).subscribe(
                data => {
                    this.isSpinner = false;
                    this._alertService.success(data, true);
                    this._router.navigate(['/login']);
                },
                error => {
                    this.isSpinner = false;
                    let er: any = JSON.parse(error._body);
                    let message = er.Message;
                    if (!message) {
                        message = error.statusText;
                    }
                    this._alertService.error(message);
                }
            );
    }

    /**
     * Faculty one change
     * @param $event
     */
    toFirstFaculty($event): void {
        this.facultiesFirst = $event.Faculties;
    }

    /**
     * Faculty two change
     * @param $event
     */
    toSecondFaculty($event): void {
        this.facultiesSecond = $event.Faculties;
    }

    /**
     * Open popup modal
     */
    openPopup(): void {
        let dialogRef = this._dialog.open(PopupComponent, {
            height: 'auto',
            width: 'auto',
            data: {readed: this.isRead}
        });
        dialogRef.afterClosed().subscribe(result => {
            this.isRead = result;
        });
    }

}
