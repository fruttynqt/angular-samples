import {Component, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


@Component({
    selector: 'popup-dialog',
    templateUrl: './popup.component.html',
    styleUrls: ['./popup.component.css']
})
export class PopupComponent {
    constructor(public dialogRef: MatDialogRef<PopupComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any) {
    }

    isOKDisabled = true;

    onElementScroll($event) {
        if ($event.target.scrollHeight == $event.target.scrollTop + $event.target.offsetHeight) this.isOKDisabled = false;
    }

    onNoClick(): void {
        this.data.isRead = true;
        this.dialogRef.close();
    }
}
