import {Component, Inject, Output, EventEmitter, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {AlertService, AuthenticationService} from '../../services/index';
import {HttpService} from "../../services/http.service";

@Component({
    moduleId: module.id,
    selector: 'login-popup',
    templateUrl: 'login-popup.component.html',
})
export class LoginPopUpComponent implements OnInit {
    @Output() failedLogin = new EventEmitter<boolean>();
    model: any = {};
    returnUrl: string;
    stepNumber: number = 1;
    maxStep: number = 5;
    errorMessage: string;
    dataForPosting = {
        Email: '',
        Code: '',
        Password: '',
        ConfirmPassword: '',
    };
    isSpinner: boolean = false;
    successfullyChangedPassword: boolean = false;

    constructor(public dialogRef: MatDialogRef<LoginPopUpComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private _route: ActivatedRoute,
                private _router: Router,
                private _authenticationService: AuthenticationService,
                private _alertService: AlertService,
                private _httpService: HttpService) {
    }

    ngOnInit() {
        this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/order';
    }

    /**
     * Close PopUp
     */
    closePopup(): void {
        this.dialogRef.close();
    }

    /**
     * Login
     */
    login() {
        this._authenticationService.login(this.model)
            .subscribe(
                data => {
                    this.closePopup();
                    this._router.navigate([this.returnUrl]);
                },
                error => {
                    this.failedLogin.emit(true);
                    this._alertService.error(error);
                });
    }

    /**
     * Step number
     */
    increaseNumber(): void {
        this.stepNumber === this.maxStep ? this.stepNumber = 1 : this.stepNumber++;
    }

    /**
     * Next step
     */
    nextStep(): void {
        switch (this.stepNumber) {
            case 1:
                this.increaseNumber();
                break;
            case 2:
                this.httpEmail('restorepassword');
                break;
            case 3:
                this.httpEmail('restorecode');
                break;
            case 4:
                this.httpEmail('newpassword', true);
                break;
            case 5: {
                this.dataForPosting.Email = '';
                this.dataForPosting.Password = '';
                this.dataForPosting.Code = '';
                this.dataForPosting.ConfirmPassword = '';
                this.successfullyChangedPassword = false;
                this.stepNumber = 2;
                break;
            }
        }

    }

    /**
     * Request http email
     * @param {string} link
     * @param {boolean} restore
     */
    httpEmail(link: string, restore: boolean = false) {
        this.isSpinner = true;
        this._httpService.postEmail(`/authorization/${link}`, this.dataForPosting)
            .subscribe(response => {
                    this.errorMessage = '';
                    this.increaseNumber();
                    this.isSpinner = false;
                    this.successfullyChangedPassword = restore;
                },
                error => {
                    this.isSpinner = false;
                    this.errorMessage = JSON.parse(error._body).Message;

                });
    }

}