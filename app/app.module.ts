import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule, BrowserXhr} from '@angular/http';
import {NoopAnimationsModule, BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BaseRequestOptions} from '@angular/http';
import {AppComponent} from './app.component';
import {PdfViewerComponent} from 'ng2-pdf-viewer';

// Directives
import {
    AlertDirective,
    EqualValidatorDirective,
    FooterDirective,
    PageDirective,
    HasRoleDirective
} from './directives/index';

// Guards
import {AuthGuard, RoleGuard} from './guards/index';

// Common components
import {
    HomeComponent,
    LoginComponent,
    LoginPopUpComponent,
    RegisterComponent,
    OrderComponent,
    StepOneComponent,
    StepTwoComponent,
    StepThreeComponent,
    StepFourComponent,
    StepFifthComponent
} from './components/index';

// Admin components
import {
    AdminComponent,
    UniversityAdminListComponent,
    UniversityPanelComponent,
    MyOrdersComponent,
    UniversityRatesComponent,
    FileLibraryComponent,
    OrdersComponent
} from './components/index';

// Popup components
import {
    PopupComponent,
    FromCloudComponent,
    PdfPopupComponent,
    AdduniversityPopupComponent,
    RatesPopupComponent,
    LibraryPopupComponent,
    PopupAdminListComponent,
    RateLibraryPopupComponent
} from './components/index';

// Services
import {routing} from './app.routing';
import {
    AlertService,
    AuthenticationService,
    HttpService,
    FooterService,
    ParseAccessTokenService,
    PagerService,
    DataSourceService,
    Database
} from './services/index';

// Pipe
import {FileSizePipe} from './pipe/file-size.pipe';
import {dateMomentPipe} from "./pipe/date-moment.pipe";

// Material
import {
    MatCheckboxModule,
    MatDatepickerModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSortModule,
    MatTableModule,
    MatTooltipModule
} from '@angular/material';
import {CdkTableModule} from '@angular/cdk/table';
import {NgProgressModule, NgProgressBrowserXhr} from 'ngx-progressbar';

@NgModule({
    exports: [
        CdkTableModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatDialogModule,
        MatSortModule,
        MatTableModule,
        MatIconModule,
        MatInputModule,
        MatRadioModule,
        MatProgressSpinnerModule,
        MatNativeDateModule,
        MatTooltipModule
    ]
})
export class materialModule {
}

@NgModule({
    declarations: [
        AppComponent,
        AlertDirective,
        PageDirective,
        FooterDirective,
        HomeComponent,
        LoginComponent,
        HasRoleDirective,
        RegisterComponent,
        OrderComponent,
        StepOneComponent,
        StepTwoComponent,
        StepThreeComponent,
        StepFourComponent,
        StepFifthComponent,
        FromCloudComponent,
        PopupComponent,
        PopupAdminListComponent,
        EqualValidatorDirective,
        FileSizePipe,
        dateMomentPipe,
        PdfViewerComponent,
        LoginPopUpComponent,
        PdfPopupComponent,
        RateLibraryPopupComponent,
        AdminComponent,
        UniversityPanelComponent,
        AdduniversityPopupComponent,
        MyOrdersComponent,
        UniversityAdminListComponent,
        UniversityRatesComponent,
        RatesPopupComponent,
        FileLibraryComponent,
        LibraryPopupComponent,
        OrdersComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        BrowserAnimationsModule,
        NoopAnimationsModule,
        routing,
        materialModule,
        ReactiveFormsModule,
        NgProgressModule
    ],
    entryComponents: [
        FromCloudComponent,
        PopupComponent,
        LoginPopUpComponent,
        PdfPopupComponent,
        AdduniversityPopupComponent,
        RatesPopupComponent,
        LibraryPopupComponent,
        PopupAdminListComponent,
        RateLibraryPopupComponent
    ],
    providers: [
        AuthGuard,
        RoleGuard,
        AlertService,
        FooterService,
        AuthenticationService,
        HttpService,
        ParseAccessTokenService,
        PagerService,
        BaseRequestOptions,
        Database,
        DataSourceService,
        {provide: BrowserXhr, useClass: NgProgressBrowserXhr},
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
