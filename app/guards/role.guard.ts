import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {AuthenticationService} from '../services/authentication.service'
import {Observable} from 'rxjs/Observable';

@Injectable()
export class RoleGuard implements CanActivate {
    constructor(private _router: Router) {
    }

    canActivate(next: ActivatedRouteSnapshot,
                state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if (AuthenticationService.role()['role'] === '1') {
            // logged in so return true
            return true;
        }
        // not logged in so redirect to login page with the return url
        this._router.navigate(['/order']);
        return false;
    }
}
