import {Injectable, Inject} from '@angular/core';
import {Http, Response, RequestOptions, Request, Headers, ResponseContentType} from '@angular/http';
import {AuthenticationService} from './authentication.service';
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import {apiUrl} from '../app.config';

@Injectable()
export class HttpService {

    constructor(private http: Http) {
    }

    /**
     * GET - method all
     * @param {string} link
     * @returns {Observable<any>}
     */
    getDataAll(link: string) {
        return this.http.get(apiUrl + link, this.getHeaders()).map(
            (response: Response) => response.json()
        );
    }

    getDataPages(link: string) {
        return this.http.get(apiUrl + link, this.getHeaders(true)).map(
            (response: Response) => {
                let pagination = JSON.parse(response.headers.get('Paging-Headers'));
                return {
                    pagination: pagination,
                    response: response.json()
                }
            }
        );
    }

    /**
     * GET - method by id
     * @param {string} link
     * @param {number} id
     * @returns {Observable<any>}
     */
    getDataById(link: string, id: number) {
        return this.http.get(apiUrl + link + '/' + id, this.getHeaders()).map(
            (response: Response) => response.json()
        );
    }

    /**
     * POST - method
     * @param {string} link
     * @param data
     * @returns {Observable<any | any>}
     */
    postData(link: string, data: any) {
        return this.http.post(apiUrl + link, data, this.getHeaders())
            .map((response: Response) => response.json())
            .catch((error: any) => Observable.throw(error));
    }

    postEmail(link: string, data: any) {
		return this.http.post(apiUrl + link, data, this.getHeaders())
			.map((response: Response) => response)
			.catch((error: any) => Observable.throw(error));
	}

    /**
     * PUT - method
     * @param {string} link
     * @param {number} id
     * @param data
     * @returns {Observable<any>}
     */
    putData(link: string, id: number, data: any) {
        return this.http.put(apiUrl + link + '/' + id, data, this.getHeaders())
            .map((response: Response) => response.json());
    }

    /**
     * Upload File
     * @param {string} link
     * @param data
     * @returns {Observable<any | any>}
     */
    uploadFile(link: string, data: any) {
        return this.http.post(apiUrl + link, data, this.getHeaders(false, 'enctype', 'multipart/form-data'))
            .map((response: Response) => response.json())
            .catch((error: any) => Observable.throw(error));
    }

    /**
     * DELETE - method
     * @param {string} link
     * @param {number} id
     * @returns {Observable<any>}
     */
    deleteData(link: string, id: number) {
        return this.http.delete(apiUrl + link + '/' + id, this.getHeaders()).map(
            (response: Response) => response.json()
        );
    }

    /**
     * Download method files
     * @param {string} link
     * @returns {Observable<any>}
     */
    download(link: string) { //get file from service
        return this.http.get(link, {
            responseType: ResponseContentType.Blob,
            headers: new Headers({'Content-Type': 'application/x-www-form-urlencoded'})
        }).map((response: Response) => response.json())
    }

    private getHeaders(pagination: boolean = false, contentType: string = 'Content-Type', type: string = 'application/json') {
        // Create authorization header with jwt token
        let headers = new Headers();
        headers.append(contentType, type);
        if (pagination) {
            headers.append('Access-Control-Expose-Headers', 'Paging-Headers');
        }

        let currentToken = AuthenticationService.auth();
        if (currentToken) {
            headers.append('PH-Bearer-Token', currentToken);
        }

        return new RequestOptions({headers: headers});
    }
}