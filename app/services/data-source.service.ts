import {Injectable, Inject} from '@angular/core';
import {AlertService} from './alert.service';
import {HttpService} from "./http.service";
import {PagerService} from "./pager.service";
import {MatSort} from '@angular/material';

import {DataSource} from '@angular/cdk/collections';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';

@Injectable()
export class Database {
    static URL: any;
    dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
    pageChange: BehaviorSubject<object> = new BehaviorSubject<object>({});

    get data(): any[] {
        return this.dataChange.value;
    }

    get page(): object {
        return this.pageChange.value;
    }

    constructor(private _httpService: HttpService,
                private _alertService: AlertService) {
        this.getBase();
    }

    getBase() {
        this._httpService.getDataPages(Database.URL)
            .subscribe(
                data => {
                    let newResponse = data.response.filter(item => {
                        if (item !== null) return item;
                    });
                    this.dataChange.next(newResponse);
                    let page = PagerService.getPager(data.pagination.TotalCount, data.pagination.CurrentPage, data.pagination.PageSize);
                    this.pageChange.next(page);
                },
                error => this._alertService.error(JSON.parse(error._body).Message)
            );
    }
}

@Injectable()
export class DataSourceService extends DataSource<any> {
    static ColumnsSort: any;

    constructor(private _database: Database, private _sort: MatSort) {
        super();
    }

    connect(): Observable<any[]> {
        const displayDataChanges = [
            this._database.dataChange,
            this._sort.sortChange,
        ];

        return Observable.merge(...displayDataChanges).map(() => {
            return this.getSortedData();
        });
    }

    getSortedData(): any[] {
        const data = this._database.data.slice();
        if (!this._sort.active || this._sort.direction == '') {
            return data;
        }

        return data.sort((a, b) => {
            let propertyA: number | string = '';
            let propertyB: number | string = '';
            DataSourceService.ColumnsSort.filter(sort => {
                if (this._sort.active === sort) {
                    return [propertyA, propertyB] = [a[sort], b[sort]];
                }
            });

            let valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            let valueB = isNaN(+propertyB) ? propertyB : +propertyB;

            return (valueA < valueB ? -1 : 1) * (this._sort.direction == 'asc' ? 1 : -1);
        });
    }

    disconnect() {
    }
}