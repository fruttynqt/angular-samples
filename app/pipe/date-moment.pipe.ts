import {Pipe, PipeTransform} from "@angular/core";
import * as moment from 'moment';

@Pipe({name: 'dateMoment'})
export class dateMomentPipe implements PipeTransform {

    transform(date: any) {
        return moment(date).format("MM.DD.YYYY");
    }
}