export class User {
    UserId: number;
    Password: string;
    NameEnglish: string;
    NameHebrew: string;
    SurnameEnglish: string;
    SurnameHebrew: string;
    PhoneWork: string;
    PhoneMobile: string;
    PhoneHome: string;
    Email: string;
    Address: string;
    UniversityFirstId: number;
    UniversityFirstFacultyId: number;
    ValidationType: number;
    Role: number;
    Birthday: number;
}
