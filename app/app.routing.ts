import {Routes, RouterModule} from '@angular/router';

// Common components
import {HomeComponent, RegisterComponent, OrderComponent, MyOrdersComponent} from './components/index';

// Admin components
import {
    AdminComponent,
    UniversityAdminListComponent,
    UniversityPanelComponent,
    UniversityRatesComponent,
    FileLibraryComponent,
    OrdersComponent
} from './components/index';

// Guards
import {AuthGuard, RoleGuard} from './guards/index';

const appRoutes: Routes = [
    {
        path: 'admin', component: AdminComponent, canActivate: [AuthGuard, RoleGuard],
        children: [
            {path: '', redirectTo: 'universities', pathMatch: 'full'},
            {path: 'universities', component: UniversityPanelComponent},
            {path: 'administrators-list', component: UniversityAdminListComponent},
            {path: 'rates', component: UniversityRatesComponent},
            {path: 'library', component: FileLibraryComponent},
            {path: 'orders', component: OrdersComponent},
        ]
    },
    {path: 'orders', redirectTo:'', component: MyOrdersComponent},
    {path: '', component: HomeComponent},
    {path: 'register', component: RegisterComponent},
    {path: 'order', component: OrderComponent, canActivate: [AuthGuard]},

    // Otherwise redirect to home.
    {path: '**', redirectTo: ''}
];

export const routing = RouterModule.forRoot(appRoutes);